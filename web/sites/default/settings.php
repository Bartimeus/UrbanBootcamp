<?php

/**
 * @file
 * Platform.sh example settings.php file for Drupal 8.
 */

// Default Drupal 8 settings.
//
// These are already explained with detailed comments in Drupal's
// default.settings.php file.
//
// See https://api.drupal.org/api/drupal/sites!default!default.settings.php/8
$databases = [];
$settings['update_free_access'] = FALSE;
$settings['container_yamls'][] = $app_root . '/' . $site_path . '/services.yml';
$settings['file_scan_ignore_directories'] = [
  'node_modules',
  'bower_components',
];

$settings["config_sync_directory"] = '../config/sync';

if (empty($_ENV['PLATFORM_BRANCH'])) {
  $config['config_split.config_split.development']['status'] = TRUE;
  $config['config_split.config_split.platform']['status'] = FALSE;
}
else {
  $config['config_split.config_split.development']['status'] = FALSE;
  $config['config_split.config_split.platform']['status'] = TRUE;
}

// Automatic Platform.sh settings.
if (file_exists($app_root . '/' . $site_path . '/settings.platformsh.php')) {
  include $app_root . '/' . $site_path . '/settings.platformsh.php';
}

// Local database settings.
if (getenv('DB_HOST')) {
  $databases['default']['default'] = [
    'database' => 'drupal',
    'username' => 'drupal',
    'password' => 'drupal',
    'prefix' => '',
    'host' => 'mariadb',
    'port' => '3306',
    'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
    'driver' => 'mysql',
  ];
  $settings['install_profile'] = 'soundtact_profile';
  $settings['hash_salt'] = 'nqsx9sZuObpTCrs-YLM0OQ5sHd_ejx0nBTbDSRLX8DeCw-HVDjR9snslz_ID6eSOxCyDsPBggA';
}
