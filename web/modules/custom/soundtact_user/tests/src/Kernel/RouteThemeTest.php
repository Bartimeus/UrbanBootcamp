<?php

namespace Drupal\Tests\soundtact_user\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Tests\field\Traits\EntityReferenceTestTrait as TraitsEntityReferenceTestTrait;
use Drupal\Tests\soundtact_user\Traits\UserTrait;

/**
 * Class to test the creation of routes and altering theme.
 */
class RouteThemeTest extends KernelTestBase {

  use TraitsEntityReferenceTestTrait;
  use UserTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'node',
    'taxonomy',
    'user',
    'field',
    'text',
    'soundtact_user',
    'field_permissions',
  ];

  /**
   * The bundle used in this test.
   *
   * @var string
   */
  protected $bundle = 'route';

  /**
   * The name of the field used in this test.
   *
   * @var string
   */
  protected $fieldName = 'field_theme';

  /**
   * The vocabulary used in this test.
   *
   * @var string
   */
  protected $vocabulary = 'theme';

  /**
   * The test user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $userWithPermissions;

  /**
   * The test user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $userWithoutPermissions;

  /**
   * The current_user service, used to login and logout.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Term used in the test.
   *
   * @var \Drupal\taxonomy\Entity\Term
   */
  protected $term1;

  /**
   * Term used in the test.
   *
   * @var \Drupal\taxonomy\Entity\Term
   */
  protected $term2;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->installEntitySchema('node');
    $this->installEntitySchema('taxonomy_term');
    $this->installEntitySchema('taxonomy_vocabulary');
    $this->installEntitySchema('user');
    $this->installSchema('system', ['sequences']);
    $this->installSchema('user', 'users_data');
    $this->installSchema('node', ['node_access']);
    $this->installConfig($this->modules);

    // Create route content type.
    $node_type = NodeType::create(['type' => $this->bundle, 'name' => 'Route']);
    $node_type->save();

    // Create a vocabulary.
    $vocabulary = Vocabulary::create([
      'name' => $this->vocabulary,
      'description' => $this->vocabulary,
      'vid' => $this->vocabulary,
    ]);
    $vocabulary->save();

    // Create a term reference field on node.
    $this->createEntityReferenceField(
      'node',
      $this->bundle,
      $this->fieldName,
      'Term reference',
      'taxonomy_term',
      'default',
      ['target_bundles' => [$this->vocabulary]],
      1
    );
    // Create a term reference field on user.
    $this->createEntityReferenceField(
      'user',
      'user',
      $this->fieldName,
      'Term reference',
      'taxonomy_term',
      'default',
      ['target_bundles' => [$this->vocabulary]],
      1
    );

    // Create some terms.
    $this->term1 = Term::create([
      'vid' => $this->vocabulary,
      'name' => 'Bootcamp',
    ]);
    $this->term1->save();

    $this->term2 = Term::create([
      'vid' => $this->vocabulary,
      'name' => 'Kunstroute',
    ]);
    $this->term2->save();

    // Create users.
    $this->userWithPermissions = $this->createUser([
      'name' => 'AdminUser',
      'uid' => 1,
      'mail' => 'example@example.com',
      'field_theme' => $this->term1->id(),
    ], [
      'access content',
      'create field_theme',
      'edit field_theme',
      'view field_theme',
    ]);
    $this->userWithoutPermissions = $this->createUser([
      'name' => 'TrainerUser',
      'uid' => 2,
      'mail' => 'example@example.com',
      'field_theme' => $this->term2->id(),
    ], ['access content']);

    $this->currentUser = $this->container->get('current_user');
  }

  /**
   * Test if the route field_theme gets changed to the users field_theme.
   */
  public function testRouteCreationWithoutPermission() {
    $this->currentUser->setAccount($this->userWithoutPermissions);
    $node = Node::create([
      'type' => $this->bundle,
      'title' => 'Route1',
      'field_theme' => NULL,
      'uid' => $this->currentUser->id(),
    ]);
    $node->save();

    $node_value = $node->get($this->fieldName)->getValue();
    $user_value = $this->userWithoutPermissions->get($this->fieldName)->getValue();

    $this->assertEquals($user_value, $node_value, 'has the same field_theme');
  }

  /**
   * Test if the route field_theme gets changed to the users field_theme.
   */
  public function testRouteCreationWithPermission() {
    $this->currentUser->setAccount($this->userWithPermissions);
    $node = Node::create([
      'type' => $this->bundle,
      'title' => 'Route1',
      'field_theme' => $this->term1->id(),
      'uid' => $this->currentUser->id(),
    ]);
    $node->save();

    $node_value = $node->get($this->fieldName)->getValue()[0]['target_id'];

    $this->assertEquals($this->term1->id(), $node_value, 'has the term 1 value');

    $this->userWithoutPermissions->set($this->fieldName, $this->term1->id());
    $this->userWithoutPermissions->save();

    $node->set('title', 'Route1 Second');
    $node->save();

    $node_value = $node->get($this->fieldName)->getValue()[0]['target_id'];

    $this->assertEquals($this->term1->id(), $node_value, 'has the same term 1 value when user field changed');
  }

}
