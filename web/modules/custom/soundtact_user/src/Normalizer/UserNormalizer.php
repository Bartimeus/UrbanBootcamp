<?php

namespace Drupal\soundtact_user\Normalizer;

use Drupal\soundtact_api\Normalizer\ContentEntityNormalizer;
use Drupal\user\UserInterface;

/**
 * Converts the Drupal entity object structures to a normalized array.
 */
class UserNormalizer extends ContentEntityNormalizer {

  /**
   * The normalizer used to normalize the typed data.
   *
   * @var \Symfony\Component\Serializer\Normalizer\NormalizerInterface
   */
  protected $serializer;

  /**
   * {@inheritdoc}
   */
  public function supportsNormalization($data, $format = NULL) {
    if (!is_object($data) || !$this->checkFormat($format)) {
      return FALSE;
    }

    if ($data instanceof UserInterface) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($entity, $format = NULL, array $context = []) {
    // Can't inject serializer due to the following error:
    // [error]  Circular reference detected for service "serializer",
    // path: "serializer -> soundtact_api.typed_data -> serializer".
    // @todo Fix this error in core?
      // @codingStandardsIgnoreStart
      /* @var $object \Drupal\Core\TypedData\TypedDataInterface */
      if (!$this->serializer) {
        $this->serializer = \Drupal::service('serializer');
      }
      // @codingStandardsIgnoreEnd

    $normalized_user = parent::normalize($entity, $format, $context);

    unset(
      $normalized_user['langcode'],
      $normalized_user['preferred_admin_langcode'],
      $normalized_user['preferred_langcode'],
      $normalized_user['mail'],
      $normalized_user['timezone'],
      $normalized_user['access'],
      $normalized_user['login'],
      $normalized_user['init'],
      $normalized_user['roles'],
      $normalized_user['field_theme']
    );

    $normalized_user['id'] = (int) $entity->id();

    return $normalized_user;
  }

}
