<?php

namespace Drupal\soundtact_point\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Point entities.
 *
 * @ingroup soundtact_point
 */
class PointEntityDeleteForm extends ContentEntityDeleteForm {


}
