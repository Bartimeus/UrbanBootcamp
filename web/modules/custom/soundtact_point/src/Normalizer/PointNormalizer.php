<?php

namespace Drupal\soundtact_point\Normalizer;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\soundtact_api\Normalizer\ContentEntityNormalizer;
use Drupal\soundtact_point\Entity\PointEntityInterface;

/**
 * Converts the Drupal entity object structures to a normalized array.
 */
class PointNormalizer extends ContentEntityNormalizer {

  /**
   * The normalizer used to normalize the typed data.
   *
   * @var \Symfony\Component\Serializer\Normalizer\NormalizerInterface
   */
  protected $serializer;

  /**
   * {@inheritdoc}
   */
  public function supportsNormalization($data, $format = NULL) {
    // If we aren't dealing with an object or the format is not supported return
    // now.
    if (!is_object($data) || !$this->checkFormat($format)) {
      return FALSE;
    }

    if ($data instanceof PointEntityInterface) {
      return TRUE;
    }
    // Otherwise, this normalizer does not support the $data object.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($entity, $format = NULL, array $context = []) {
    // Can't inject serializer due to the following error:
    // [error]  Circular reference detected for service "serializer",
    // path: "serializer -> soundtact_api.typed_data -> serializer".
    // @todo Fix this error in core?
    // @codingStandardsIgnoreStart
    /* @var $object \Drupal\Core\TypedData\TypedDataInterface */
    if (!$this->serializer) {
      $this->serializer = \Drupal::service('serializer');
    }
    // @codingStandardsIgnoreEnd

    $normalized_point = parent::normalize($entity, $format, $context);

    unset(
      $normalized_point['user_id']
    );

    $this->normalizeBeacon($entity, $normalized_point);
    $this->normalizeAudio($entity, $normalized_point);

    return $normalized_point;
  }

  /**
   * Normalize the beacon.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param array $normalized_point
   *   The array which gets returned.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function normalizeBeacon(ContentEntityInterface $entity, array &$normalized_point) {
    if ($beacon_field = $entity->beacon) {
      $beacon_storage = $this->entityTypeManager->getStorage('beacon_entity');
      $beacon = $beacon_storage->load($beacon_field->getValue()[0]['target_id']);
      $normalized_point['beacon'] = $this->serializer->normalize($beacon);
    }
  }

  /**
   * Normalize the audio.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param array $normalized_point
   *   The array which gets returned.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function normalizeAudio(ContentEntityInterface $entity, array &$normalized_point) {
    if ($audio_field = $entity->audio_reference) {
      $media_storage = $this->entityTypeManager->getStorage('media');
      $audio = $media_storage->load($audio_field->getValue()[0]['target_id']);
      $normalized_point['audio_reference'] = $this->serializer->normalize($audio);
    }
  }

}
