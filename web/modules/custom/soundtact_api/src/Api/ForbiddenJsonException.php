<?php

namespace Drupal\soundtact_api\Api;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * JsonException class for AccessDenied exceptions.
 */
class ForbiddenJsonException extends JsonResponse {

  /**
   * JsonExceptionResponse constructor.
   *
   * @param string $message
   *   The message that should be shown in the jsonresponse.
   */
  public function __construct(string $message) {
    /** @var array $data */
    $data = [
      'code' => 401,
      'message' => $message,
    ];

    parent::__construct($data, 401);
  }

}
