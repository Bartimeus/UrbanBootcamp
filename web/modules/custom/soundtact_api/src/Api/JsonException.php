<?php

namespace Drupal\soundtact_api\Api;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * JsonException class for AccessDenied exceptions.
 */
class JsonException extends JsonResponse {

  /**
   * JsonExceptionResponse constructor.
   *
   * @param string $message
   *   The message that should be shown in the jsonresponse.
   * @param string $error
   *   The machine error name.
   */
  public function __construct(string $message, string $error = 'exception') {
    /** @var array $data */
    $data = [
      'error' => $error,
      'code' => 400,
      'message' => $message,
    ];

    parent::__construct($data, 400);
  }

}
