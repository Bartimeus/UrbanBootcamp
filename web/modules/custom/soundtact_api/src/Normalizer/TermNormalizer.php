<?php

namespace Drupal\soundtact_api\Normalizer;

use Drupal\serialization\Normalizer\ContentEntityNormalizer;
use Drupal\taxonomy\TermInterface;

/**
 * Normalizes all terms.
 */
class TermNormalizer extends ContentEntityNormalizer {

  /**
   * The interface or class that this Normalizer supports.
   *
   * @var string
   */
  protected $supportedInterfaceOrClass = 'Drupal\taxonomy\TermInterface';

  /**
   * {@inheritdoc}
   */
  public function supportsNormalization($data, $format = NULL) {
    // If we aren't dealing with an object or the format is not supported return
    // now.
    if (!is_object($data) || !$this->checkFormat($format)) {
      return FALSE;
    }

    if ($data instanceof TermInterface) {
      return TRUE;
    }
    // Otherwise, this normalizer does not support the $data object.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($entity, $format = NULL, array $context = []) {
    $normalized_entity = parent::normalize($entity);

    // We want to unset fields here which we don't need.
    unset(
      $normalized_entity['uuid'],
      $normalized_entity['weight'],
      $normalized_entity['parent'],
      $normalized_entity['changed'],
      $normalized_entity['default_langcode'],
      $normalized_entity['metatag'],
      $normalized_entity['path'],
      $normalized_entity['description'],
      $normalized_entity['vid']
    );

    return $normalized_entity;
  }

}
