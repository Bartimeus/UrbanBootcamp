<?php

namespace Drupal\soundtact_beacon;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Beacon entities.
 *
 * @ingroup soundtact_beacon
 */
class BeaconEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Beacon ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\soundtact_beacon\Entity\BeaconEntity $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.beacon_entity.edit_form',
      ['beacon_entity' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
