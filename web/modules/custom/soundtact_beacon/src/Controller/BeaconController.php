<?php

namespace Drupal\soundtact_beacon\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\soundtact_api\Api\AccessDeniedJsonException;
use Drupal\soundtact_api\Api\ForbiddenJsonException;
use Drupal\soundtact_api\Api\JsonException;
use Drupal\soundtact_api\Api\NotFoundJsonException;
use Drupal\soundtact_beacon\Entity\BeaconEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;

/**
 * Logic for the Route api.
 */
class BeaconController extends ControllerBase {
  /**
   * Constant for logging.
   *
   * @var string
   *   Constant.
   */
  const LOG_NAME = 'soundtact_beacon';

  /**
   * The request from the user.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   *   Logger Interface.
   */
  protected $logger;

  /**
   * Symfony\Component\Serializer\Serializer definition.
   *
   * @var \Symfony\Component\Serializer\Serializer
   */
  protected $serializer;

  /**
   * BeaconController constructor.
   *
   * @param \Symfony\Component\HttpFoundation\Request $currentRequest
   *   The current request.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   Logger interface.
   * @param \Symfony\Component\Serializer\Serializer $serializer
   *   The serializer to normalize classes.
   */
  public function __construct(Request $currentRequest, LoggerChannelFactoryInterface $logger, Serializer $serializer) {
    $this->currentRequest = $currentRequest;
    $this->logger = $logger->get(self::LOG_NAME);
    $this->serializer = $serializer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('logger.factory'),
      $container->get('serializer')
    );
  }

  /**
   * Get a list of all the beacons.
   *
   * @return \Drupal\soundtact_route\Api\AccessDeniedJsonException|JsonResponse
   *   A json object with the beacons.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getList() {
    if (!$this->currentUser()->hasPermission('access content')) {
      return new AccessDeniedJsonException($this->t('You have no access to routes.'));
    }

    // Init storages.
    $beacon_storage = $this->entityTypeManager()->getStorage('beacon_entity');

    // Get the theme query parameter to base the query on the route theme.
    $query_path = 'geolocation_field.';
    $query = $beacon_storage->getQuery()
      ->condition($query_path . 'lat', $this->currentRequest->query->get('latMin'), '>=')
      ->condition($query_path . 'lat', $this->currentRequest->query->get('latMax'), '<=')
      ->condition($query_path . 'lng', $this->currentRequest->query->get('lngMin'), '>=')
      ->condition($query_path . 'lng', $this->currentRequest->query->get('lngMax'), '<=');

    // Get all route entities.
    $beacon_ids = $query->execute();

    // Load all the entities.
    $beacons = $beacon_storage->loadMultiple($beacon_ids);
    $return_array = [];

    foreach ($beacons as $key => $beacon) {
      try {
        $name = $beacon->get('name')->getValue()[0]['value'];
        $id = $beacon->id();
        $geolocation_field = $beacon->get('geolocation_field')->getValue();
        $lat = $geolocation_field[0]['lat'];
        $lng = $geolocation_field[0]['lng'];

        $return_array[$key] = [
          'id' => (int) $id,
          'name' => $name,
          'lat' => (float) $lat,
          'lng' => (float) $lng,
        ];
      }
      catch (\Exception $e) {
        $return_array[$key] = [
          'error' => [
            'code' => $e->getCode(),
            'message' => $e->getMessage(),
          ],
        ];
      }
    }

    $response = new JsonResponse(['beacons' => $return_array], 200);
    return $response;
  }

  /**
   * Check if the current beacon exits in Drupal.
   */
  public function checkExists() {
    if (!$this->currentUser()->hasPermission('edit beacon entities')) {
      return new ForbiddenJsonException($this->t('You have no access to check beacons'));
    }

    // Init storages.
    $beacon_storage = $this->entityTypeManager()->getStorage('beacon_entity');

    $query = $beacon_storage->getQuery()
      ->condition('major', $this->currentRequest->query->get('major'))
      ->condition('minor', $this->currentRequest->query->get('minor'))
      ->condition('mac_address', $this->currentRequest->query->get('mac_address'))
      ->range(0, 1);

    $beacon_ids = $query->execute();

    if (count($beacon_ids) > 0) {
      $beacon_id = array_values($beacon_ids)[0];
      $beacon = $beacon_storage->load($beacon_id);
      $beacon_name = $beacon->get('name') !== NULL ? $beacon->name->getValue()[0]['value'] : $this->t('No name in database.');
      $geolocation_field = $beacon->get('geolocation_field')->getValue();
      $lat = $geolocation_field[0]['lat'];
      $lng = $geolocation_field[0]['lng'];
      $distance = $beacon->get('distance')->getValue()[0]['value'];
      return new JsonResponse([
        "id" => (int) $beacon->id(),
        "name" => $beacon_name,
        "lat" => (float) $lat,
        "lng" => (float) $lng,
        "distance" => (int) $distance,
      ]);
    }

    return new NotFoundJsonException($this->t('Beacon has not been found in the system.'));
  }

  /**
   * Update a existing beacon.
   *
   * @param \Drupal\soundtact_beacon\Entity\BeaconEntityInterface $beacon
   *   The beacon entity.
   */
  public function updateBeacon(BeaconEntityInterface $beacon) {
    if (!$this->currentUser()->hasPermission('edit any beacon content')) {
      return new ForbiddenJsonException($this->t('You have no access to add beacons.'));
    }

    $body_parameters = [
      'name',
      'lat',
      'lng',
      'distance',
    ];

    $body = json_decode($this->currentRequest->getContent());
    foreach ($body_parameters as $parameter) {
      if (empty($body->$parameter)) {
        return new JsonException($this->t('The query parameter @parameter has not been found in the request. Add @parameter to the url.', ['@parameter' => $parameter]), 'invalid_query_parameters');
      }
    }

    $beacon->setName($body->name);
    $beacon->set('geolocation_field', [[
      'lat' => $body->lat,
      'lng' => $body->lng,
    ],
    ]);
    $beacon->set('distance', $body->distance);

    try {
      $beacon->save();
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
      return new JsonException($this->t('Error occured when saving the beacon. Please try again.'), 'save_error');
    }

    return new JsonResponse([
      'success' => TRUE,
      'beacon' => $this->serializer->normalize($beacon),
    ]);
  }

  /**
   * Route to add a beacon to Drupal.
   */
  public function addBeacon() {
    if (!$this->currentUser()->hasPermission('add beacon entities')) {
      return new ForbiddenJsonException($this->t('You have no access to add beacons.'));
    }

    $body_parameters = [
      'major',
      'minor',
      'lat',
      'lng',
      'mac_address',
      'distance',
    ];

    $body = json_decode($this->currentRequest->getContent());
    foreach ($body_parameters as $parameter) {
      if (empty($body->$parameter)) {
        return new JsonException($this->t('The query parameter @parameter has not been found in the request. Add @parameter to the url.', ['@parameter' => $parameter]), 'invalid_query_parameters');
      }
    }

    // Check if current beacon already exists.
    $beacon_storage = $this->entityTypeManager()->getStorage('beacon_entity');
    $query = $beacon_storage->getQuery()
      ->condition('major', $body->major)
      ->condition('minor', $body->minor)
      ->condition('mac_address', $body->mac_address)
      ->range(0, 1);
    $beacon_ids = $query->execute();

    if (count($beacon_ids) > 0) {
      return new JsonException($this->t('Beacon already exists in database.'), 'beacon_already_exists');
    }

    // Format data.
    $body->geolocation_field = [];
    $body->geolocation_field[] = [
      'lat' => $body->lat,
      'lng' => $body->lng,
    ];
    unset($body->lat);
    unset($body->lng);

    // Add new beacon.
    /** @var \Drupal\soundtact_beacon\Entity\BeaconEntityInterface $new_beacon */
    $new_beacon = $beacon_storage->create((array) $body);

    try {
      $new_beacon->save();
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
      return new JsonException($this->t('Error occured when saving the beacon. Please try again.'), 'save_error');
    }

    // Set name if needed.
    if (empty($new_beacon->getName())) {
      $new_beacon->name = 'Beacon ' . $new_beacon->id();
      try {
        $new_beacon->save();
      }
      catch (\Exception $e) {
        $this->logger->error($e->getMessage());
        return new JsonException($this->t('Error occured when changing name. Please try again.'), 'save_error');
      }
    }

    return new JsonResponse([
      'success' => TRUE,
      'beacon' => $this->serializer->normalize($new_beacon),
    ]);
  }

}
