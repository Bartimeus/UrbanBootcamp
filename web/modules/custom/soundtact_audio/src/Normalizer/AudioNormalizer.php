<?php

namespace Drupal\soundtact_audio\Normalizer;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeRepositoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Url;
use Drupal\serialization\Normalizer\ContentEntityNormalizer;

/**
 * Normalizes all terms.
 */
class AudioNormalizer extends ContentEntityNormalizer {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The filesystem.
   *
   * @var Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs an EntityNormalizer object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeRepositoryInterface $entity_type_repository
   *   The entity type repository.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   File system.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityTypeRepositoryInterface $entity_type_repository, EntityFieldManagerInterface $entity_field_manager, FileSystemInterface $file_system) {
    parent::__construct($entity_type_manager, $entity_type_repository, $entity_field_manager);
    $this->entityTypeManager = $entity_type_manager;
    $this->fileSystem = $file_system;
  }

  /**
   * The interface or class that this Normalizer supports.
   *
   * @var string
   */
  protected $supportedInterfaceOrClass = 'Drupal\media\MediaInterface';

  /**
   * {@inheritdoc}
   */
  public function normalize($entity, $format = NULL, array $context = []) {
    $normalized_entity = parent::normalize($entity);

    $normalized_entity['id'] = $normalized_entity['mid'];

    $file_id = $entity->field_media_audio_file->getValue()[0]['target_id'];
    $file = $this->entityTypeManager->getStorage('file')->load($file_id);
    $uri = $file->getFileUri();
    $normalized_entity['url'] = Url::fromUri(file_create_url($uri))->toString();
    $normalized_entity['name_extension'] = $this->fileSystem->basename($file->getFileUri());

    // We want to unset fields here which we don't need.
    unset($normalized_entity['uuid'],
      $normalized_entity['mid'],
      $normalized_entity['name'],
      $normalized_entity['langcode'],
      $normalized_entity['vid'],
      $normalized_entity['bundle'],
      $normalized_entity['revision_created'],
      $normalized_entity['revision_user'],
      $normalized_entity['revision_log_message'],
      $normalized_entity['status'],
      $normalized_entity['thumbnail'],
      $normalized_entity['uid'],
      $normalized_entity['created'],
      $normalized_entity['changed'],
      $normalized_entity['default_langcode'],
      $normalized_entity['revision_translation_affected'],
      $normalized_entity['metatag'],
      $normalized_entity['path'],
      $normalized_entity['field_media_audio_file'],
      $normalized_entity['field_theme']);

    return $normalized_entity;
  }

}
